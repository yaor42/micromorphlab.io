Micromorph website
==================

-  `Live site <https://micromorph.gitlab.io>`_

Development
-----------

1. Install requirements as a user or in a virtualenv

   -  As a user (visible elsewhere)

      .. code:: console

         pip install --user -r requirements.txt

   -  Using virtualenv (only visible when activated)

      .. code:: console

         virtualenv VENV                  # only needed once
         . VENV/bin/activate              # once per session
         pip install -r requirements.txt  # in case of new requirements

2. Edit files using your favorite text editor and rebuild using ``make html``. It’s a good idea to
   install a reStructuredText plugin for your editor.  For syntax, see examples in this repository
   and the `reStructuredText Primer <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_.

3. Open/reload pages in your browser, starting at ``_build/html/index``.

Publishing
----------

Pushing to a branch will run the test suite. Pushes to branch ``main``
will deploy to the live site.
